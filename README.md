https://en.wikipedia.org/wiki/Markdown

There is no official Markdown specification and there are many different \
implementations.\
This seems intended by the creators of the language.

#### TODO

- delph deeper into different Markdown implementations (like CommonMark)
- add support for "Setext-style headers"
- maybe remove pound signs infront of headers
